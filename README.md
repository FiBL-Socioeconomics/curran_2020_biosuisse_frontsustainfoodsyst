The files and folders contained in this repository allow the main analyses of the paper "Representative farm-based sustainability assessment of the organic sector in Switzerland using the SMART-Farm Tool", published in the journal "Frontiers in Sustainable Food Systems", doi: 10.3389/fsufs.2020.554362. The files are explained below.

ZIP FILE:

**MS554362_RMD_Script.zip**: This ZIP file is simply a duplicate of all files and folders presented individually in the repository. These files are described below. To run the analysis script, simply download this file, extract it and run the R Markdown script.

INDIVIDUAL FILES:

**rmd_BioSuisseSustain_PCA_MS554362_All.Rmd**: This is the main analysis script in R Markdown language. This can be opened in R Studio. It contains a mix of explanation text and R script chunks to repeat the main analyses of the paper. When you run the code chunks or "knit" the entire script, ensure that the file structure is identical to that presented in the repository, that file paths are relative and not absolute and that all packages called in the script are locally installed in your version of R.

**rmd_BioSuisseSustain_PCA_MS554362_All.docx**: This is the resulting final report in DOCX format that should be produced by the R Markdown script. This file will be overwritten everytime you knit the R Markdown script. Knitting is a terminology for running the entire Markdown script, including all embedded code chunks, to produce a final report.

**Input folder**: This folder contains the required data files in CSV format. The folder and its contents must be maintained exactly as named and positioned as they are called in the Markdown script. Therefore do not modify the folder or its files.

**Output folder**: The resulting data tables will end up in this folder. Dta outputs are restricted only to the final indicator list and their weights for each theme. Otherwise all results of the script are outputted to the DOCX file listed above (rmd_BioSuisseSustain_PCA_MS554362_All.docx)

